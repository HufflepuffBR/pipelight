Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Pipelight
Upstream-Contact: Michael Mueller <michael@fds-team.de>
Source: http://fds-team.de/cms/projects.html

Files: *
Copyright:	2013, Michael Mueller <michael@fds-team.de>
			2013, Sebastian Lackner <sebastian@fds-team.de>
License: MPL-1.1 or GPL-2 or LGPL-2.1

Files: src/*
Copyright:	1998, Netscape Communications Corporation
			1998, Josh Aas <josh@mozilla.com>
			2013, Michael Mueller <michael@fds-team.de>
			2013, Sebastian Lackner <sebastian@fds-team.de>
License: MPL-1.1 or GPL-2 or LGPL-2.1

Files: src/npapi-headers/*
Copyright:	1998, Netscape Communications Corporation
			1998, Josh Aas <josh@mozilla.com>
License: MPL-1.1 or GPL-2 or LGPL-2.1

License: MPL-1.1
 The contents of this file are subject to the Mozilla Public License Version
 1.1 (the "License"); you may not use this file except in compliance with
 the License. You may obtain a copy of the License at
 http://www.mozilla.org/MPL/
 .
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the
 License.
 .
 The Original Code is mozilla.org code.
 .
 The Initial Developer of the Original Code is
 Netscape Communications Corporation.
 Portions created by the Initial Developer are Copyright (C) 1998
 the Initial Developer. All Rights Reserved.
 .
 Contributor(s):
   Josh Aas <josh@mozilla.com>
   Michael Müller <michael@fds-team.de>
   Sebastian Lackner <sebastian@fds-team.de>
 .
 Alternatively, the contents of this file may be used under the terms of
 either the GNU General Public License Version 2 or later (the "GPL"), or
 the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 in which case the provisions of the GPL or the LGPL are applicable instead
 of those above. If you wish to allow use of your version of this file only
 under the terms of either the GPL or the LGPL, and not to allow others to
 use your version of this file under the terms of the MPL, indicate your
 decision by deleting the provisions above and replace them with the notice
 and other provisions required by the GPL or the LGPL. If you do not delete
 the provisions above, a recipient may use your version of this file under
 the terms of any one of the MPL, the GPL or the LGPL.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or 
 modify it under the terms of the GNU Lesser General Public 
 License as published by the Free Software Foundation; either 
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, 
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public 
 License along with this library; if not,  write to the Free Software 
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.